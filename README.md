# RFID230-2-python-driver
driver for RFID230-2 card reader

Test Environment: unittest


**How to use:**

simply edit your encryption key and do _main.Driver().loop()_ to read data from card.